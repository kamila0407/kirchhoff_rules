#include "mainwindow.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <QtCore>
#include <QtGui>
#include <QInputDialog>
#include <QMessageBox>

using namespace std;
#define FINAL 10
#define ADD_CAP 1
#define ADD_RES 2
#define ADD_EMF 3
#define ADD_LINE 4
#define ADD_LINE_2 5
int numb=0, numb_1 =0;
int pointIndex =-1, elemIndex = -1;
int ALL_D = 0;
int final = 0;
int state_l = 0;
///MyWidget - Класс, в котором происходит вся работа программы за исключением прорисовки кнопок, наследуется от QWidget.
class MyWidget : public QWidget
{
public:
    ///paintEvent - основная функция для рисования изображений.
    /**Функция, которая в зависимости от типа необходимого элемента вызывает другую функцию,
     *  которая рисует соотвутствующий элемент: конденсатор, резистор, ЭДС или линию, соединяющую их.
     @param *event - объект типа QPaintEvent*/

    virtual void paintEvent(QPaintEvent *event)
    {
        for (int i = 0; i <= elemIndex; ++i) {
            if(elemType[i]==ADD_CAP)
            {
                paintCapacitor(event, i);
                state = 0;
            }
            if (elemType[i]==ADD_RES)
            {
                 paintResistor(event, i);
                 state = 0;
            }
            if(elemType[i]==ADD_EMF)
            {
                paintEmf(event, i);
                state = 0;
            }
        }
        for (int j =0; j<numb; j++)
        {
            if(state_point[j] == ADD_LINE_2)
            {
                paintLine(event, j);
                state_p = 0;
            }
        }

    }
    ///paintCapacitor - рисование конденсатора
    /** Функция, которая рисует конденсатор, координаты которого соответствуют координатам клика мыши пользователя.
     * @param *event - объект типа QPaintEvent.
     * @param index - целое число, с помощью которого определяется, координаты какого именно клика мыши использует функция для рисования элемента.*/
    virtual void paintCapacitor(QPaintEvent *event, int index)
    {
        QPainter p(this);
        p.setPen(QPen(QColor(0xFF, 0x14, 0x93), 3));
        p.drawLine(X[index],Y[index]-30,X[index],Y[index]+30);
        p.drawLine(X[index]+20,Y[index]-30,X[index]+20,Y[index]+30);
        p.setPen(QPen(QColor(0x6A, 0x5A, 0xCD), 10));
        p.drawPoint(Point[index*2][1], Point[index*2][2]);
        p.drawPoint(Point[index*2+1][1], Point[index*2+1][2]);
    }
    ///paintResistor - рисование резистора
    /** Функция, которая рисует резистор, координаты которого соответствуют координатам клика мыши пользователя.
     * @param *event - объект типа QPaintEvent.
     * @param index - целое число, с помощью которого определяется, координаты какого именно клика мыши использует функция для рисования элемента.*/
    virtual void paintResistor(QPaintEvent *event, int index)
    {
        QPainter p(this);
        p.setPen(QPen(QColor(0xFF, 0x14, 0x93), 3));
        p.drawRect(X[index], Y[index]-30, 90, 60);
        p.setPen(QPen(QColor(0x6A, 0x5A, 0xCD), 10));
        p.drawPoint(Point[index*2][1], Point[index*2][2]);
        p.drawPoint(Point[index*2+1][1], Point[index*2+1][2]);
    }
    ///paintEmf - рисование ЭДС
    /** Функция, которая рисует ЭДС, координаты которой соответствуют координатам клика мыши пользователя.
     * @param *event - объект типа QPaintEvent.
     * @param index - целое число, с помощью которого определяется, координаты какого именно клика мыши использует функция для рисования элемента.*/
    virtual void paintEmf(QPaintEvent *event, int index)
    {
        QPainter p(this);
        p.setPen(QPen(QColor(0xFF, 0x14, 0x93), 3));
        p.drawLine(X[index],Y[index]-30,X[index],Y[index]+30);
        p.drawLine(X[index]+20,Y[index]-50,X[index]+20,Y[index]+50);
        p.setPen(QPen(QColor(0x6A, 0x5A, 0xCD), 10));
        p.drawPoint(Point[index*2][1], Point[index*2][2]);
        p.drawPoint(Point[index*2+1][1], Point[index*2+1][2]);
    }
    ///paintLine - рисование линии
    /** Функция, которая рисует конденсатор, координаты которого соответствуют координатам клика мыши пользователя.
     * @param *event - объект типа QPaintEvent.
     * @param index - целое число, с помощью которого определяется, координаты какого именно клика мыши использует функция для рисования элемента.*/
    virtual void paintLine(QPaintEvent *event, int index)
    {
        QPainter p(this);
        p.setPen(QPen(QColor(0x6A, 0x5A, 0xCD), 3));
        p.drawLine(x_line[index-1],y_line[index-1],x_line[index], y_line[index]);
    }

    int state;
    ///addCapasitor - добавление конденсатора
    /** В переменную state типа int кладется значение флага ADD_CAP, чтобы paintEvent вызвал функию, рисующую конденсатор.*/
    void addCapasitor()
    {
        state = ADD_CAP;
    }
    ///addResistor - добавление резистора
    /** В переменную state типа int кладется значение флага ADD_RES, чтобы paintEvent вызвал функию, рисующую резистор.*/
    void addResistor()
    {
        state = ADD_RES;
    }
    ///addEmf - добавление ЭДС
    /** В переменную state типа int кладется значение флага ADD_EMF, чтобы paintEvent вызвал функию, рисующую ЭДС.*/
    void addEmf()
    {
        state = ADD_EMF;
    }
    int state_p = 0;
    ///addLine - добавление линии
    /** В переменную state_p типа int кладется значение флага ADD_LINE, чтобы paintEvent вызвал функию, рисующую линию, соединяющую два элемента.*/
    void addLine()
    {
        state_p = ADD_LINE;
    }
    ///Calculate - подсчет суммарного тока
    /** По школьному Закону Кирхгофа считается значение тока в цепи. Для этого вычисляется суммарное сопротивление и суммарное напряжение.
     * На экране появляется окошко, информирующее о том, что ток был вычеслен и выводит его численное значение в Амперах*/
    void Calculate ()
    {
        int sum_1 = 0, sum_2 = 0, sum_3 = 0;
        double current;
        for (int i = 0; i< numb_1 + 1; i++)
        {
            if (el_t[i]==1)
                {sum_1+=input[i];}
            if (el_t[i]==2)
                {sum_2+=input[i];}
            if (el_t[i]==3)
                {sum_3+=input[i];}
        }
        current = (sum_3 - sum_1)/sum_2;
        QMessageBox msgBox;
        msgBox.setInformativeText(QString("Total current has been calculated: %1 A").arg(current));
        msgBox.exec();
    }
    ///Compare_func - функция сравнения двух пар координат
    /**Функция принимает 4 координаты и попарно сравнивает их, определяя, находятся ли они достаточно близко друг к другу.
     * @param int x - x-координата клика мыши.
     * @param int y - y-координата клика мыши.
     * @param int n - x-координата существующей точки.
     * @param int m - y-координата существующей точки.
     * @return 1, если координаты клика мыши совпали с достаточной точностью с координатами существующей точки; 0 в обратном случае.*/
    int Compare_func(int x,int y,int n,int m)
    {
        if ((x <= n+5) && (x >=n-5) && (y>=m-5) && (y<=m+5))
            {return 1;}
        else
            {return 0;}
    }
    ///Compare_double_func_cap - специальная функция сравнения двух пар координат,используется для конденсатора и ЭДС.
    /**Функция принимает 4 координаты и определяет, находится ли первая пара координат (координаты двойного клика мыши) внутри одного из элементов - конденсатора или ЭДС.
     * Началом отсчета координат внутри элемента является первая точка каждого элемента, координаты именно этой точки участвуют в сравнении с координатами двойного клика мыши.
     * @param int x - x-координата двойного клика мыши.
     * @param int y - y-координата двойного клика мыши.
     * @param int n - x-координата существующей точки конденсатора или ЭДС.
     * @param int m - y-координата существующей точки конденсатора или ЭДС.
     * @return 1, если координаты двойного клика мыши находятся внутри конденсатора или ЭДС; 0 в обратном случае.*/
    int Compare_double_func_cap(int x,int y,int n,int m)
    {
        if ((x <= n+29) && (x >=n+1) && (y>=m-10) && (y<=m+10))
            {return 1;}
        else
            {return 0;}
    }
    ///Compare_double_func_res - специальная функция сравнения двух пар координат, используется для резистора.
    /**Функция принимает 4 координаты и определяет, находится ли первая пара координат (координаты двойного клика мыши) внутри резистора.
     * Началом отсчета координат внутри элемента является первая точка каждого элемента, координаты именно этой точки участвуют в сравнении с координатами двойного клика мыши.
     * @param int x - x-координата двойного клика мыши.
     * @param int y - y-координата двойного клика мыши.
     * @param int n - x-координата существующей точки резистора.
     * @param int m - y-координата существующей точки резистора.
     * @return 1, если координаты двойного клика мыши находятся внутри резистора; 0 в обратном случае.*/
    int Compare_double_func_res(int x,int y,int n,int m)
    {
        if ((x <= n+60) && (x >=n+30) && (y>=m-10) && (y<=m+10))
            {return 1;}
        else
            {return 0;}
    }
    ///mousePressEvent - функция для обработки события клика мыши.
    /**Функция решает две важнейшие задачи: добавление элемента и соединение двух точек элемента.
     * 1.Добавление элемента.
     *      а)Функция сравнивает переменную state со значениями флагов ADD_CAP, ADD_RES и ADD_EMF.
     *      б)Если state равно одному из значений флагов, происходит :
     *              * выделение памяти под массивы координат элемента, под массив типа элемента, под массив точек.
     *              * заполнение всех массивов соответствующими значениями, причем координаты элемента - координаты события клика мыши, а остальные массивы заполняются в соответствии с типом элемента.
     * 2.Добавление линии.
     *      а)Функция  сравнивает переменную state_p со значением флага ADD_LINE.
     *      б)Если state_p равно значению ADD_LINE, происходит :
     *              *выделение памяти под массивы координат точек,которые будут соединены.
     *              *с помощью флага определяется, данная точка является первой из двух, которые будт соединены, или второй; поэтому функция разбивается на две ситуации, но в каждой из них происходят почти одинаковые события.
     *              *в цикле происходит поиск существующей точки, находящийся достаточно близко к точке, координаты которой - координаты события клика мыши.(вызывается функция Compare_func).
     *              *если такая точка найдена, то массивы координат точек заполняются координатами существующей точки, а координатами события клика мыши.(это делается специально для того, чтобы соединялись настоящие точки элементов, чтобы цепь была визуально непрерывной).
     *              *если такая точка не найдена, то есть пользователь недостаточно аккуратен, на экране появляется окошко с дальнейшими инструкциями и происходит выход из функции, причем если точка была второй из двух,которые должны были быть соединены,координаты первой точки из этой пары удаляются).
     * @param *event - объект типа QPaintEvent*/

    virtual void mousePressEvent(QMouseEvent *event)
    {
        if (ADD_CAP == state || ADD_RES == state|| ADD_EMF == state)
        {
            elemIndex++;
            pointIndex+=2;
            if (elemIndex==0)
            {
                X.resize(1);
                Y.resize(1);
                elemType.resize(1);
                Point.resize(2);
                for (int i = 0; i < 2; i++)
                {
                    Point[i].resize(3);
                }
            }
            else
            {
                X.resize(elemIndex + 1);
                Y.resize(elemIndex + 1);
                elemType.resize(elemIndex + 1);
                Point.resize((elemIndex +1)*2);
                for (int i = pointIndex - 1; i < pointIndex + 1; i++)
                {
                    Point[i].resize(3);
                }
            }
            elemType[elemIndex] = state;
            X[elemIndex] = event->x();
            Y[elemIndex] = event->y();
//Обработка первой точки элемента
            Point[pointIndex-1][0] = elemType[elemIndex];
            Point[pointIndex-1][1] = event->x();
            Point[pointIndex-1][2] = event->y();
//Обработка второй точки элемента
            Point[pointIndex][0] = elemType[elemIndex];
            if(elemType[elemIndex]==ADD_CAP || elemType[elemIndex]==ADD_EMF)
            {
                Point[pointIndex][1] = (event->x())+20;
                Point[pointIndex][2] = event->y();
            }
            if(elemType[elemIndex]==ADD_RES)
            {
                Point[pointIndex][1] = (event->x())+90;
                Point[pointIndex][2] = event->y();
            }
        }
//Обработка нажатия мыши на существующую точку
        int k,l,found;
        if(ADD_LINE == state_p)
        {
            if(numb == 0)
            {
                x_line.resize(1);
                y_line.resize(1);
                state_point.resize(1);
            }
            else
            {
                x_line.resize(numb + 1);
                y_line.resize(numb + 1);
                state_point.resize(numb + 1);
            }
            int x = event->x();
            int y = event->y();
            if (0 == state_l)
            {
                for (k = 0; k<pointIndex+1; k++)
                {
                    found = Compare_func(x, y, Point[k][1], Point[k][2]);
                    if (found == 1)
                    {
                        x_line[numb] = Point[k][1];
                        y_line[numb] = Point[k][2];
                        break;
                    }
                    if (found ==0 && k == pointIndex)
                    {
                        QMessageBox msgBoxError;
                        msgBoxError.setInformativeText("No matches found, try again,please.\nPress connectButton and continue.");
                        msgBoxError.exec();

                        return;
                    }
                }
                state_point[numb] = state_p;
                    numb++;
                state_l = 1;
                return;
            }
            if (1 == state_l)
            {
                for (k = 0; k<pointIndex+1; k++)
                {
                    found = Compare_func(x, y, Point[k][1], Point[k][2]);
                    if (found == 1)
                    {
                        x_line[numb] = Point[k][1];
                        y_line[numb] = Point[k][2];
                        break;
                    }
                    if (found ==0 && k == pointIndex)
                    {
                        QMessageBox msgBoxError;
                        msgBoxError.setInformativeText("No matches found, try again,please.\nPress connectButton and continue.\n");
                        msgBoxError.exec();
                        state_l = 0;
                        numb--;
                        return;
                    }
                }
                state_point[numb] = state_p+1;
                numb++;
                state_l = 0;
                state_point[numb-1] = ADD_LINE_2;
            }
        }
        update();
    }

    ///mousePressEvent - функция для обработки события двойного клика мыши.
    /**Функция проверяет,собрана ли схема с помощью флага. Если собрана, то происходит запись численных значений элементов.
     * Выделяется память под массивы с координатами события двойного клика мыши, под массив значений и массив типа элемента.
     * В цикле по каждой первой точке элемента сравниваются координаты события и координаты точки и определяется, находятся ли первые внутри одного из элементов : конденсатора или ЭДС.(вызывается функция Compare_double_func_cap).
     * Если находятся, то перед пользователем всплывает окошко, предлагающее ввести численное значение элемента в соответсвующих единицах измерения.
     * Если элемент, значение которого хочет записать пользователь, не конденсатор и не ЭДС, происходит такое же сравнение координат, но уже в поисках соответствующего резистора.(вызывается функция Compare_double_func_res).
     * В случае успеха, всплывает в точности почти такое же окошко, как и в первом случае, за исключением единиц измерения.
     * Введенные значения записываются в массив значений, и массив типа элемента заполняется соотвественно.
     * Также существует обработка ошибки : если пользователь неточно выбрал место двойного клика мыши, всплывает информирующее об этом окошко и предлагает попробовать снова.*/
    virtual void mouseDoubleClickEvent(QMouseEvent *event)
    {
        int found_double = 0;
        if (ALL_D == 1)
        {
            if(numb_1 ==0)
            {
                x_value.resize(1);
                y_value.resize(1);
                input.resize(1);
                el_t.resize(1);
            }
            else
            {
                x_value.resize(numb_1 + 1);
                y_value.resize(numb_1 + 1);
                input.resize(numb_1 + 1);
                el_t.resize(numb_1 + 1);
            }
            int flag = 0, flag_2 =0;
            x_value[numb_1] = event->x();
            y_value[numb_1] = event->y();
            for (int k = 0; k<pointIndex+1; k+=2)
            {
                found_double = Compare_double_func_cap(x_value[numb_1],y_value[numb_1], Point[k][1], Point[k][2]);
                if (found_double == 1 && (Point[k][0]==1 || Point[k][0]==3))
                {
                    if(Point[k][0]==1)
                    {
                        double a = QInputDialog::getDouble(this, QString("Enter charge"), "Value in coulombs:");
                        double b = QInputDialog::getDouble(this, QString("Enter capacity"), "Value in farads:");
                        input[numb_1] = a/b;
                    }
                    if (Point[k][0]==3)
                    {
                        input[numb_1] = QInputDialog::getDouble(this, QString("Enter voltage"), "Value in volts:");
                    }
                    el_t[numb_1] = Point[k][0];
                    flag_2 = 1;
                    break;
                }
                if (found_double == 0)
                {

                    found_double = Compare_double_func_res(x_value[numb_1],y_value[numb_1], Point[k][1], Point[k][2]);
                    if (found_double == 1 && Point[k][0]==2)
                    {
                        input[numb_1] = QInputDialog::getDouble(this, QString("Enter resistance"), "Value in ohms:");
                        el_t[numb_1] = Point[k][0];
                        flag = 1;
                        break;
                    }
                }
            }
                if (found_double == 0 && flag == 0)
                {
                    flag_2 = 2;
                    QMessageBox msgBoxError2;
                    msgBoxError2.setInformativeText("No matches found, try again,please.\n");
                    msgBoxError2.exec();
                }
            if (flag_2 != 2)
            {
                numb_1 ++;}
        }
    }
private:
    vector<int> X;
    vector<int> Y;
    vector<int> elemType;
    vector<int> state_point;
    vector<vector<int>> Point;
    vector<int> x_line;
    vector<int> y_line;
    vector<int> el_t;
    vector<double> input;
    vector<int> x_value;
    vector<int> y_value;
};
///MyWindow - Класс, в котором присходит определение внешнего вида программы, прорисовка кнопок.
MyWindow::MyWindow(QWidget *parent) : QDialog(parent)
{
    resize(1700,900);
    capacitor = new QPushButton("&Capacitor");
    resistor = new QPushButton("&Resistor");
    emf = new QPushButton("&EMF");
    ok = new QPushButton("&OK!");
    ok->setDefault(true);
    calculate = new QPushButton("&Calculate");
    calculate->setDefault(true);
    connectButton = new QPushButton("&Connect");
    exitButton = new QPushButton("&Quit");
    lbl = new QLabel("&Enter");
    line = new QLineEdit;
    lbl->setBuddy(line);

    QHBoxLayout *lower = new QHBoxLayout;
    lower->addWidget(connectButton,1);
    lower->addWidget(ok,1);
    lower->addWidget(calculate, 1);
    lower->addWidget(exitButton, 1);

    QVBoxLayout *left = new QVBoxLayout;
    left->addStretch(1);
    left->addWidget(capacitor);
    left->addWidget(resistor);
    left->addWidget(emf);
    left->addStretch(1);

    QHBoxLayout *right = new QHBoxLayout;
    scheme = new MyWidget;
    right->addWidget(scheme, 1);

    QHBoxLayout *upper = new QHBoxLayout;
    upper->addLayout(left);
    upper->addLayout(right);

    QVBoxLayout *main1 = new QVBoxLayout;
    main1->addLayout(upper);
    main1->addLayout(lower);

    setLayout(main1);
    setWindowTitle("Kirchhoff rules project");

    QObject::connect(exitButton, &QPushButton::clicked, this, &MyWindow::quit);
    QObject::connect(capacitor, &QPushButton::clicked, this, &MyWindow::CapClicked);
    QObject::connect(resistor, &QPushButton::clicked, this, &MyWindow::ResClicked);
    QObject::connect(emf, &QPushButton::clicked, this, &MyWindow::EmfClicked);
    QObject::connect(connectButton, &QPushButton::clicked, this, &MyWindow::ConClicked);
    QObject::connect(ok, &QPushButton::clicked, this, &MyWindow::OkClicked);
    QObject::connect(calculate, &QPushButton::clicked, this, &MyWindow::CalkClicked);
}
///quit - обработка события клика кнопки quit.
/** После нажатия кнопки quit программа завершается и выходит.*/
void MyWindow::quit(bool clicked)
{
    exit(0);
}
///CapClicked - обработка события клика кнопки capacitor.
/** После нажатия кнопки capacitor вызывается функция добавления конденсатора.*/
void MyWindow::CapClicked()
{
    scheme->addCapasitor();
}
///ResClicked - обработка события клика кнопки resistor.
/** После нажатия кнопки resistor вызывается функция добавления резистора.*/
void MyWindow::ResClicked()
{
    scheme->addResistor();
}
///EmfClicked - обработка события клика кнопки emf.
/** После нажатия кнопки emf вызывается функция добавления ЭДС.*/
void MyWindow::EmfClicked()
{
    scheme->addEmf();
}
///OkClicked - обработка события клика кнопки ok.
/** После нажатия кнопки ok в глобальную переменную ALL_D кладется 1, чтобы сообщить программе, что схема собрана.*/
void MyWindow::OkClicked()
{
    ALL_D = 1;
}
///CalkClicked - обработка события клика кнопки calculate.
/** После нажатия кнопки calculate вызывается функция, которая считает численное значение тока в цепи.*/
void MyWindow::CalkClicked()
{
    scheme->Calculate();
}
///ConClicked - обработка события клика кнопки connect.
/** После нажатия кнопки connect вызывается функция, которая добавляет линию,соединяющую два элемента в схему.*/
void MyWindow::ConClicked()
{
    scheme->addLine();
}
