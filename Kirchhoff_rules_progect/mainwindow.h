#ifndef TUT_HEAD_1_2_H
#define TUT_HEAD_1_2_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>

class MyWidget;

class MyWindow : public QDialog
{
    Q_OBJECT

public:
    MyWindow(QWidget *parent=0);
private:
    QPushButton *capacitor;
    QPushButton *resistor;
    QPushButton *emf;
    QPushButton *ok;
    QPushButton *calculate;
    QPushButton *exitButton;
    QPushButton *connectButton;
    QPushButton *add;
    MyWidget *scheme;
    QLabel *lbl;
    QLineEdit *line;
    void quit(bool clicked=false);
    void CapClicked();
    void ResClicked();
    void EmfClicked();
    void OkClicked();
    void CalkClicked();
    void ExClicked();
    void ConClicked();
    void AddClicked();
    void TextChanged(QString str);
};


#endif // TUT_HEAD_1_2_H
